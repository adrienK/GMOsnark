const TAGS = {
	'' : ['<em>','</em>'],
	_ : ['<strong>','</strong>'],
	'~' : ['<s>','</s>'],
	'\n' : ['<br />'],
	' ' : ['<br />'],
	'-': ['<hr />']
};

/** Outdent a string based on the first indented line's leading whitespace
 *	@private
 */
function outdent(str) {
	return str.replace(RegExp('^'+(str.match(/^(\t| )+/) || '')[0], 'gm'), '');
}

/** Encode special attribute characters to HTML entities in a String.
 *	@private
 */
function encodeAttr(str) {
	return (str+'').replace(/"/g, '&quot;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
}

/** Parse Markdown into an HTML String. */
export default function parse(md, prevLinks) {
	let tokenizer = /((?:^|\n+)(?:- ?|_|(?:\* ?)){3,}(?:$|\n+))|(?:^((?:```|:::) *\w*)\n([\s\S]*?)\n(?:```|:::)$)|((?:(?:^|\n+)(?:\t|  {2,})[^<\-+* ].+)+\n*)|((?:(?:^|\n)( *[>*+-]+(?! ?[*-])|\d+\.) +.+)(?:\n.+)*)|(?:\!\[([^\]]*?)\]\(([^\)]+?)\))|(\[)|(\](?:\(([^\)]+?)\))?)|(?:(?:^|\n+)([^\s<].*)\n(\-{3,}|={3,})(?:\n+|$))|(?:(?:^|\n+)(#{1,6})\s*(.+)(?:\n+|$))|(?:`(.*?)`)|(  \n\n*|\n{2,}|__|\*\*|[_*]|~~)|(^\d+\\..*)|((?:(?:^|\n+)(?:\|.*))+)|(<?((?:https?:\/\/.+|\S+@\S+)\.\w+.*)>?)|(\\[\\`*_{}[\]()#+-.!])|(\((?:[cC]|[rR]|[pP]|tm|TM)\)|\+-)|[^=:]("([^"\n]*)"|'([^'\n]*)')|(:([^\s:]+?):)/gm,
		context = [],
		out = '',
		links = prevLinks || {},
		last = 0,
		chunk, prev, token, inner, t;

	function tag(token) {
		var desc = TAGS[token.replace(/\*/g,'_')[1] || ''],
			end = context[context.length-1]==token;
		if (!desc) return token;
		if (!desc[1]) return desc[0];
		context[end?'pop':'push'](token);
		return desc[end|0];
	}

	function flush() {
		let str = '';
		while (context.length) str += tag(context[context.length-1]);
		return str;
	}

	md = md.replace(/^\[(.+?)\]:\s*(.+)$/gm, (s, name, url) => {
		links[name.toLowerCase()] = url;
		return '';
	}).replace(/^\n+|\n+$/g, '');

	while ( (token=tokenizer.exec(md)) ) {
		prev = md.substring(last, token.index);
		last = tokenizer.lastIndex;
		chunk = token[0];
		if (prev.match(/[^\\](\\\\)*\\$/)) {
			// escaped
		}
		// Code/Indent blocks:
		else if (token[3] || token[4]) {
			token[2] = (token[2]?token[2]:'')
			t = token[2].replace(/^[`:]{3} ?/g, '');

			if (token[2].match(/\:/)) {
				inner = outdent(encodeAttr(token[3]).replace(/^\n+|\n+$/g, ''));
				chunk = '<p class="code '+t.toLowerCase()+'">'+inner+'</p>';
			} else {
				inner = outdent(encodeAttr(token[3] || token[4]).replace(/^\n+|\n+$/g, ''));
				chunk = '<pre class="code"><code class="'+(token[4]?'poetry':t.toLowerCase())+'">'+inner+'</code></pre>';
			}
		}
		// > Quotes, -* lists:
		else if (token[6]) {
			t = token[6];
            inner = token[5];

			if (t==='>') {
                t = 'blockquote';
			    inner = parse(outdent(inner.replace(/^\s*[>]/gm, '')));
            } else {
				inner = inner.replace(/\[ ?\]/gm, '<input class="task-list item checkbox" type="checkbox" disabled>');
				inner = inner.replace(/\[x\]/gm, '<input class="task-list item checkbox" type="checkbox" checked disabled>');

				if (t.match(/\./)) {
					var v = token[5].replace(/^(\d+)(.*|\n)*/gm, '$1');
					inner = inner.replace(/^\d+/gm, '');
					t = t + ' start="' + v + '"';
				}

				t = t.match(/\./) ? 'ol' : 'ul';

				inner = inner.replace(/^[+\-*.] (.*)(\n|$)/gm, '<li>$1</li>\n');
				inner = inner.replace(/^  /gm, '');

				if (inner.match(/\s+[*-+]/))
				    inner = parse(outdent(inner));
            }

			chunk = '<'+t+'>' + inner + '</'+t+'>';
		}
		// Images:
		else if (token[8]) {
			t = token[8];
			if (t.match(/ ".*"$/)) {
				var v = t.replace(/.* "(.*)"$/gm, '$1');
				t = t.replace(/(.*) ".*"$/gm, '$1');
				chunk = `<img src="${encodeAttr(t)}" title="${encodeAttr(v)}" alt="${encodeAttr(token[7])}">`;
			} else {
				chunk = `<img src="${encodeAttr(t)}" alt="${encodeAttr(token[7])}">`;
			}
		}
		// Links:
		else if (token[10]) {
			if (token[11]) t = token[11];
            		else t = '';

			if (t.match(/ ".*"$/)) {
				var v = t.replace(/.* "(.*)"$/gm, '$1');
				t = t.replace(/(.*) ".*"$/gm, '$1');
				out = out.replace('<a>', `<a title="${encodeAttr(v)}" href="${encodeAttr(t || links[prev.toLowerCase()])}">`);
			} else {
				out = out.replace('<a>', `<a href="${encodeAttr(token[11] || links[prev.toLowerCase()])}">`);
			}
			chunk = flush() + '</a>';
		}
		else if (token[9]) {
			chunk = '<a>';
		}
		// Headings:
		else if (token[12] || token[14]) {
			t = 'h' + (token[14] ? token[14].length : (token[13][0]==='='?1:2));
			chunk = '<'+t+'>' + parse(token[12] || token[15], links) + '</'+t+'>';
		}
		// `code`:
		else if (token[16]) {
			chunk = '<code>'+encodeAttr(token[16])+'</code>';
		}
		// Inline formatting: *em*, **strong** & friends
		else if (token[17] || token[1]) {
			chunk = tag(token[17] || '--');
		}
		// Not a list
		else if (token[18]) {
			chunk = token[18].replace(/^(\d+)\\./gm, '$1.');
		}
		// Tables
		else if (token[19]) {
			var l = token[19].split('\n'),
					i = l.length,
					table = '',
					r = 'td>';
			while ( i-- ) {
				if(l[i].match(/^\|\s+---+.*$/)) {
					r = 'th>';
					continue;
				}
				var c = l[i].split(/\|\s*/),
						j = c.length,
						tr = '';
				while (j--) {
					tr = (c[j] ? `<${r+parse(c[j])}</${r}` : '') + tr;
				}
				table = `<tr>${tr}</tr>` + table;
				r = 'td>';
			}
			chunk = `<table>${table}</table>`;
		}
		// Auto links
		else if (token[20]) {
			t = token[21]
			if (t.match(/@/)) t = 'mailto:' + t;
			chunk = `<a href="${encodeAttr(t)}">${encodeAttr(token[21])}</a>`;
		}
		// Backslash Escapes
		else if (token[22]) {
			chunk = token[22].replace(/\\([\\`*_{}[\]()#+-.!])/gm, '$1');
		}
		// Typographer
		else if (token[23]) {
			var map = {'(c)' : '&copy;', '(r)' : '&reg;', '(p)' : '&sect;', '(tm)' : '&trade;', '+-' : '&plusmn;'};
			chunk = map[token[23].toLowerCase()];
		}
		// Typographer
		else if (token[24]) {
			if (token[25])
				chunk = '&ldquo;' + token[25] + '&rdquo;';
			else
				chunk = '&lsquo;' + token[26] + '&rsquo;';
		}
		// Emoji
		else if (token[27]) {
			chunk = '<span class="emoji icon ' + token[28] + '"></span>';
		}
		out += prev;
		out += chunk;
	}

	return (out + md.substring(last) + flush()).trim();
}
