<p align="center">
  <img src="https://git.iglou.eu/adrien/GMOsnark/raw/branch/master/logo.png" width="256" height="256" alt="GMOsnark">
</p>

GMOsnark is a dead simple \[Markdown\] parser, forked from [snarkdown](https://github.com/developit/snarkdown)

It's designed to be as minimal as possible, for constrained use-cases where a full Markdown parser would be inappropriate.

## Features
- **Fast:** since it's basically one regex and a huge if statement
- **Tiny:** it's 1kb of gzipped ES3
- **Simple:** pass a Markdown string, get back an HTML string
- **Note:** Tables are supported

## Usage
GMOsnark exports a single function, which parses a string of Markdown and returns a String of HTML. Couldn't be simpler.

Inside an HTML page

```html
<script type="module">
import toHtml from "./index.js";

let md = document.getElementById("imput").value;
let html = toHtml(md);
document.getElementById("output").innerHTML = html;
</script>

```

## License
This program is under MIT license [[read the entire license file]](https://git.iglou.eu/adrien/GMOsnark/src/branch/master/LICENSE)
